/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Kushrenada
 */
@Entity
public class Comment implements Serializable{
    
    private static final long serialVersionUID = 7L;
    
    private Long id;
    private String comment;
    private User user;
    private Pengumuman pengumuman;
    
    public Comment() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "comment_id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "peng_id")
    public Pengumuman getPengumuman() {
        return pengumuman;
    }

    public void setPengumuman(Pengumuman pengumuman) {
        this.pengumuman = pengumuman;
    }
}
