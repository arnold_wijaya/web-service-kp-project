/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Kushrenada
 */
@Entity
@Table(name = "matakuliah", catalog = "iec_announcement")
public class MataKuliah implements Serializable {
    
    private static final long serialVersionUID = 7L;
    
    private Long id;
    private String kodeMk;
    private String namaMk;
    private String sks;
    private User dosen;
    
    public MataKuliah() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_mk")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "kode_mk")
    public String getKodeMk() {
        return kodeMk;
    }

    public void setKodeMk(String kodeMk) {
        this.kodeMk = kodeMk;
    }

    @Column(name = "nama_mk")
    public String getNamaMk() {
        return namaMk;
    }

    public void setNamaMk(String namaMk) {
        this.namaMk = namaMk;
    }

    @Column(name = "sks")
    public String getSks() {
        return sks;
    }

    public void setSks(String sks) {
        this.sks = sks;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dosen_id")
    public User getDosen() {
        return dosen;
    }
    
    public void setDosen(User dosen) {
        this.dosen = dosen;
    }
}
