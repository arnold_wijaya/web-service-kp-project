/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.model;

import java.io.*;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author Kushrenada
 */
@Embeddable
public class SeenId implements Serializable {
    
    private static final long serialVersionUID = 7L;
    
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;
    
    @ManyToOne(fetch = FetchType.LAZY)
    private Pengumuman pengumuman;
    
    public SeenId() {
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.user);
        hash = 97 * hash + Objects.hashCode(this.pengumuman);
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || this != o || getClass() != o.getClass()) {
            return false;
        }
        
        SeenId other = (SeenId) o;
        
        // Cek order
        if (user == null) {
            if (other.user != null) {
                return false;
            }
        } else { 
            if (other.user == null) {
                return false;
            } else if (!user.getId().equals(other.user.getId())) {
                return false;
            }
        }
        
        // Cek item
        if (pengumuman == null) {
            if (other.pengumuman != null) {
                return false;
            }
        } else { 
            if (other.pengumuman == null) {
                return false;
            } else if (!pengumuman.getId().equals(other.pengumuman.getId())) {
                return false;
            }
        }
        
        return true;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Pengumuman getPengumuman() {
        return pengumuman;
    }

    public void setPengumuman(Pengumuman pengumuman) {
        this.pengumuman = pengumuman;
    }
}
