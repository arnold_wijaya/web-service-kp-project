/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.model;

import java.util.*;
import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Kushrenada
 */
@Entity
@Table(name = "kelas", catalog = "iec_announcement")
public class Kelas implements Serializable {

    private static final long serialVersionUID = 7L;

    private long id;
    private String kelas;
    private String semester;
    private int semester_mk;
    private MataKuliah mk;

    public Kelas() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_kelas")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "kelas")
    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    @Column(name = "semester")
    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    @Column(name = "semester_MK")
    public int getSemester_mk() {
        return semester_mk;
    }

    public void setSemester_mk(int semester_mk) {
        this.semester_mk = semester_mk;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_mk")
    public MataKuliah getMk() {
        return mk;
    }

    public void setMk(MataKuliah mk) {
        this.mk = mk;
    }

}
