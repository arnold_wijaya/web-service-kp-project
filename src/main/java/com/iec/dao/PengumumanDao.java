/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.dao;

import com.iec.hibernate.HibernateUtil;
import com.iec.model.Pengumuman;
import java.util.List;
import org.hibernate.*;

/**
 *
 * @author Kushrenada
 */
public class PengumumanDao {

    public Pengumuman findOne(Long id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Pengumuman pengumuman = null;

        try {
            pengumuman = (Pengumuman) session.get(Pengumuman.class, id);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

        return pengumuman;
    }

    public List<Pengumuman> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Pengumuman> pengumumans = null;

        try {
            Query query = session.createQuery("from Pengumuman");
            pengumumans = query.list();
            for (Pengumuman pengumuman : pengumumans) {
                pengumuman.getCreatedAt().toString();
                pengumuman.setSeens(null);
                pengumuman.setComments(null);
                pengumuman.setUser(null);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

        return pengumumans;
    }

    public Pengumuman save(Pengumuman pengumuman) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(pengumuman);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return pengumuman;
    }

    public Pengumuman update(Pengumuman pengumuman) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.update(pengumuman);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return pengumuman;
    }

    public Pengumuman delete(Pengumuman pengumuman) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.delete(pengumuman);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return pengumuman;
    }
}
