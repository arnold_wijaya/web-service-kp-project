/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.dao;

import com.iec.hibernate.HibernateUtil;
import com.iec.model.User;
import java.util.List;
import org.hibernate.*;

/**
 *
 * @author Kushrenada
 */
public class UserDao {
<<<<<<< HEAD
    public User login(String username, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = null;
        try {
            Query query = session.createQuery("from User u where u.username = :username and u.password = :password");
            query.setParameter("username", username);
            query.setParameter("password", password);
            user = (User) query.uniqueResult();
            
            user.setComments(null);
            user.setPengumumans(null);
            user.setMataKuliahs(null);
            user.setSeens(null);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }
=======

>>>>>>> 5c8708c43f59fb556068ac907cb424ba6d91f973
    public User findOne(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = null;

        try {
            //Transaction tx = session.beginTransaction();
//            Query query = session.createQuery("from Users where id = :id");
//            query.setParameter("id", id);

            user = (User) session.get(User.class, id);
            // harus diginikan karena lazy load
            user.setComments(null);
            user.setPengumumans(null);
            user.setMataKuliahs(null);
            user.setSeens(null);

        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

        return user;
    }

    public User login(String username, String password) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = null;
        try {
            Query query = session.createQuery("from User u where u.username = :username and u.password = :password");
            query.setParameter("username", username);
            query.setParameter("password", password);
            user = (User) query.uniqueResult();
            
            user.setComments(null);
            user.setPengumumans(null);
            user.setMataKuliahs(null);
            user.setSeens(null);
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }

    public User findOne(String username) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = null;

        try {
            //Transaction tx = session.beginTransaction();
            Query query = session.createQuery("from User where username = :username");
            query.setParameter("username", username);

            user = (User) query.uniqueResult();
            // harus diginikan karena lazy load
            user.setComments(null);
            user.setPengumumans(null);
            user.setMataKuliahs(null);
            user.setSeens(null);

        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

        return user;
    }

    public List<User> findAll() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<User> users = null;

        try {
            //Transaction tx = session.beginTransaction();
            Query query = session.createQuery("from User");

            users = query.list();

            for (User user : users) { // harus diginikan karena lazy load
                user.setComments(null);
                user.setPengumumans(null);
                user.setMataKuliahs(null);
                user.setSeens(null);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

        return users;
    }

    public User save(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.save(user);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return user;
    }

    public User update(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.update(user);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return user;
    }

    public User delete(User user) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;

        try {
            tx = session.beginTransaction();
            session.delete(user);
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();

            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }

        return user;
    }
}
