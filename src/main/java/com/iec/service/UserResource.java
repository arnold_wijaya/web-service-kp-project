package com.iec.service;

import com.iec.dao.UserDao;
import com.iec.model.User;
import java.util.List;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import sun.security.rsa.RSASignature;

/**
 *
 * @author Kushrenada
 */
@Path("/users")
public class UserResource {

    @Path("/{id}")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response show(@PathParam("id") Integer id) {
        UserDao ud = new UserDao();
        User user = ud.findOne(id);

        return Response.ok(user).build();
    }

    @Path("")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAll() {
        UserDao ud = new UserDao();
        List<User> users = ud.findAll();
        for (User user : users) {
            user.setPassword("");
        }
<<<<<<< HEAD
=======
        return Response.ok(users)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                .header("Access-Control-Max-Age", "1209600")
                .build();
    }

    @Path("/getall")
    @POST
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAll2() {
        UserDao ud = new UserDao();
        List<User> users = ud.findAll();
        for (User user : users) {
            user.setPassword("");
        }
>>>>>>> 5c8708c43f59fb556068ac907cb424ba6d91f973
        return Response.ok(users)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                .header("Access-Control-Max-Age", "1209600")
                .build();
    }

    @Path("/login")
    @POST
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response login(User user) {
        UserDao ud = new UserDao();
        User dariDB = ud.login(user.getUsername(), user.getPassword());
        dariDB.setPassword("");
        return Response.ok(dariDB)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                .header("Access-Control-Max-Age", "1209600")
                .build();
    }

    @Path("")
    @POST
    //@Consumes(value = MediaType.APPLICATION_JSON)
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response save(User user, @Context UriInfo uriInfo) {
        UserDao ud = new UserDao();
        ud.save(user);

        UriBuilder builder = uriInfo.getAbsolutePathBuilder();
        builder.path(Integer.toString(user.getId()));

        return Response.created(builder.build()).build();
    }

    @Path("/login")
    @POST
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response login(User user) {
        UserDao ud = new UserDao();
        User dariDB = ud.login(user.getUsername(), user.getPassword());
        dariDB.setPassword("");
        return Response.ok(dariDB)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                .header("Access-Control-Max-Age", "1209600")
                .build();
    }

    @Path("/{id}")
    @PUT
    @Consumes(value = MediaType.APPLICATION_JSON)
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response update(User user) {
        UserDao ud = new UserDao();
        ud.update(user);

        return Response.ok(user).build();
    }

    @Path("/{id}")
    @DELETE
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Integer id) {
        UserDao ud = new UserDao();
        User user = ud.findOne(id);
        if (user != null) {
            ud.delete(user);

            return Response.ok(user).build();
        } else {
            return Response.status(422).build(); // unprocessable entity
        }
    }
}
