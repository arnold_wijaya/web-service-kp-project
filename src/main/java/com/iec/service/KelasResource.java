/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.iec.service;

import com.iec.dao.KelasDao;
import com.iec.model.Kelas;
import java.util.List;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

/**
 *
 * @author Kushrenada
 */
@Path("/kelas")
public class KelasResource {

    @Path("/{id}")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response show(@PathParam("id") Integer id) {
        KelasDao kd = new KelasDao();
        Kelas kelas = kd.findOne(id);

        return Response.ok(kelas)
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
                .header("Access-Control-Max-Age", "1209600")
                .build();
    }

    @Path("")
    @GET
    @Produces(value = MediaType.APPLICATION_JSON)
    public Response getAll() {
        KelasDao ud = new KelasDao();
        List<Kelas> kelases = ud.findAll();

        return Response.ok(kelases).build();
    }
}
