/*
SQLyog Ultimate v8.55 
MySQL - 5.6.20 : Database - iec_announcement
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`iec_announcement` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `iec_announcement`;

/*Table structure for table `comments` */

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `comment_id` int(10) NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  `user_id` int(10) NOT NULL,
  `peng_id` int(10) NOT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `user_id` (`user_id`,`peng_id`),
  KEY `peng_id` (`peng_id`),
  CONSTRAINT `commentPeng_fk` FOREIGN KEY (`peng_id`) REFERENCES `pengumumans` (`peng_id`),
  CONSTRAINT `commentUser_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `comments` */

/*Table structure for table `kelas` */

DROP TABLE IF EXISTS `kelas`;

CREATE TABLE `kelas` (
  `id_kelas` int(10) NOT NULL AUTO_INCREMENT,
  `kelas` char(10) NOT NULL,
  `semester` varchar(10) NOT NULL,
  `semester_MK` int(10) NOT NULL,
  `id_mk` int(10) NOT NULL,
  PRIMARY KEY (`id_kelas`),
  KEY `id_mk` (`id_mk`),
  CONSTRAINT `kelasMK_fk` FOREIGN KEY (`id_mk`) REFERENCES `matakuliah` (`id_mk`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `kelas` */

insert  into `kelas`(`id_kelas`,`kelas`,`semester`,`semester_MK`,`id_mk`) values (1,'a','ganjil',1,1),(2,'b','ganjil',1,1),(3,'a','ganjil',1,2),(4,'b','ganjil',1,2),(5,'a','ganjil',3,3),(6,'b','ganjil',3,3);

/*Table structure for table `matakuliah` */

DROP TABLE IF EXISTS `matakuliah`;

CREATE TABLE `matakuliah` (
  `id_mk` int(10) NOT NULL AUTO_INCREMENT,
  `kode_mk` varchar(50) NOT NULL,
  `nama_mk` varchar(50) NOT NULL,
  `sks` int(10) NOT NULL,
  `dosen_id` int(11) NOT NULL,
  PRIMARY KEY (`id_mk`),
  KEY `dosen_id` (`dosen_id`),
  KEY `dosen_id_2` (`dosen_id`),
  CONSTRAINT `mkUser_fk` FOREIGN KEY (`dosen_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `matakuliah` */

insert  into `matakuliah`(`id_mk`,`kode_mk`,`nama_mk`,`sks`,`dosen_id`) values (1,'IF-1','web desain',2,2),(2,'IF-2','algoritma',3,4),(3,'IF-3','basis data',4,4),(4,'TI-1','Kalkulus',2,6);

/*Table structure for table `pengumumans` */

DROP TABLE IF EXISTS `pengumumans`;

CREATE TABLE `pengumumans` (
  `peng_id` int(10) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `tipe` varchar(10) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(10) NOT NULL,
  `id_kelas` int(10) NOT NULL,
  PRIMARY KEY (`peng_id`),
  KEY `user_id` (`user_id`,`id_kelas`),
  KEY `id_kelas` (`id_kelas`),
  CONSTRAINT `pengKelas_fk` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`),
  CONSTRAINT `pengUsers_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `pengumumans` */

insert  into `pengumumans`(`peng_id`,`judul`,`deskripsi`,`tipe`,`timestamp`,`user_id`,`id_kelas`) values (1,'Tugas Algo','buat tugas yang sudah dibahas dikelas dan dikumpulkan minggu depan','tugas','2015-12-15 20:40:12',2,2),(2,'UTS basdat','uts minggu depan di jam matakuliah, bahan dari pertemuan 1 sampai pertemuan 6, bersifat closed book','reminder','2015-12-15 20:33:43',4,5),(3,'Tugas Algo','buat tugas yang sudah dibahas dikelas dan dikumpulkan minggu depan\r\n','tugas','2015-12-15 20:33:59',2,1),(4,'UTS basdat','uts minggu depan di jam matakuliah, bahan dari pertemuan 1 sampai pertemuan 6, bersifat closed book','reminder','2015-12-15 20:33:49',4,6);

/*Table structure for table `seens` */

DROP TABLE IF EXISTS `seens`;

CREATE TABLE `seens` (
  `user_id` int(10) NOT NULL,
  `peng_id` int(10) NOT NULL,
  KEY `user_id` (`user_id`,`peng_id`),
  KEY `peng_id` (`peng_id`),
  CONSTRAINT `seensPeng_fk` FOREIGN KEY (`peng_id`) REFERENCES `pengumumans` (`peng_id`),
  CONSTRAINT `seensUser_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `seens` */

/*Table structure for table `subscribes` */

DROP TABLE IF EXISTS `subscribes`;

CREATE TABLE `subscribes` (
  `subscribe_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `id_kelas` int(10) NOT NULL,
  PRIMARY KEY (`subscribe_id`),
  KEY `user_id` (`user_id`,`id_kelas`),
  KEY `id_kelas` (`id_kelas`),
  CONSTRAINT `subsKelas_fk` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`),
  CONSTRAINT `subsUser_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `subscribes` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `level` int(10) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`user_id`,`username`,`password`,`name`,`level`) values (1,'admin','bukanadmin','admin announce',0),(2,'dosen1','passdosen','dosen satu',1),(3,'mahasiswa1','passmhs','mhs satu',2),(4,'dosen2','passdosen','dosen dua',1),(5,'mahasiswa2','passmhs','mhs dua',2),(6,'dosen3','passdosen','dosen tiga',1),(7,'otongganteng','otongajah','Otong',2);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
